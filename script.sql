create table persons(id integer, name varchar(30),patronymic varchar(30),surname varchar(30),car_id integer,city_id integer)
create table cars(id integer, model varchar(30), num varchar(30), color varchar(30), class varchar(30));
create table cities(id integer, name varchar(30));

insert into cars(id,model,num,color,class)values(1,'AUDI','x123xx196RU','RED','C');
insert into cars(id,model,num,color,class)values(2,'AUDI','a234et174RU','WHITE','D');
insert into cars(id,model,num,color,class)values(3,'WV','c426ax750RU','BLACK','B');
insert into cars(id,model,num,color,class)values(4,'MB','a404aa123RU','GREY','S');
insert into cars(id,model,num,color,class)values(5,'VAZ','a109mm66RU','YELLOW','B');
insert into cars(id,model,num,color,class)values(10,'RENAULT','h239kp96RU','GREEN','B');
insert into cars(id,model,num,color,class)values(9,'PEUGEOT','o120ee96RU','BLACK','B');
insert into cars(id,model,num,color,class)values(8,'KIA','x450bk96RU','WHITE','C');
insert into cars(id,model,num,color,class)values(7,'FORD','b098yp196RU','GREY','C');
insert into cars(id,model,num,color,class)values(6,'SKODA','a478bx196RU','WHITE','C');
 
insert into cities(id,name)values(1,'Yekaterinburg');
insert into cities(id,name)values(2,'Moscow');
insert into cities(id,name)values(3,'St.Peterburg');
insert into cities(id,name)values(4,'Perm');
insert into cities(id,name)values(5,'Tyumen');
insert into cities(id,name)values(6,'Chelyabinsk');
insert into cities(id,name)values(7,'Omsk');
insert into cities(id,name)values(8,'Kazan');
insert into cities(id,name)values(9,'Vladivostok');
insert into cities(id,name)values(10,'Norilsk');
 
insert into persons(id,name,patronymic,surname,car_id,city_id)values(1,'ivan','ivanovich','chehov',1,10);
insert into persons(id,name,patronymic,surname,car_id,city_id)values(2,'olga','sergeevna','zhukova',5,1);
insert into persons(id,name,patronymic,surname,car_id,city_id)values(3,'anton','petrovich','borodach',8,1);
insert into persons(id,name,patronymic,surname,car_id,city_id)values(4,'sergey','petrovich','chehov',3,6);
insert into persons(id,name,patronymic,surname,car_id,city_id)values(5,'olga','andreevna','ivanova',7,4);
insert into persons(id,name,patronymic,surname,car_id,city_id)values(6,'evgeniy','aleksandrovich','kobylin',2,9);
insert into persons(id,name,patronymic,surname,car_id,city_id)values(7,'anna','vladimirovna','frolova',6,1);
insert into persons(id,name,patronymic,surname,car_id,city_id)values(8,'denis','aleksandrovich','potapov',4,7);
insert into persons(id,name,patronymic,surname,car_id,city_id)values(9,'andrey','vasilyevich','bagaev',9,3);
insert into persons(id,name,patronymic,surname,car_id,city_id)values(10,'yuliya','stepanovna','chernova',10,8);


-- SECURITY --
/*  add into server.xml
<!--
        <Realm className="org.apache.catalina.realm.UserDatabaseRealm"
               resourceName="UserDatabase"/>
      </Realm>
-->
<Realm  className="org.apache.catalina.realm.JDBCRealm"
             driverName="org.postgresql.Driver"
          connectionURL="jdbc:postgresql://localhost:5432/postgres"
         connectionName="postgres" connectionPassword="postgres"
              userTable="tomcat_users" userNameCol="user_name" userCredCol="password"
          userRoleTable="tomcat_users_roles" roleNameCol="role_name" />
</Realm>

*/
CREATE TABLE tomcat_users (
  user_name varchar(20),
  password varchar(32)
);
CREATE TABLE tomcat_roles (
  role_name varchar(20)
);
CREATE TABLE tomcat_users_roles (
  user_name varchar(20),
  role_name varchar(20)
);
INSERT INTO tomcat_users (user_name, password) VALUES ('deron', 'deronpass');
INSERT INTO tomcat_users (user_name, password) VALUES ('larry', 'buythecompetition');
INSERT INTO tomcat_roles (role_name) VALUES ('dude');
INSERT INTO tomcat_roles (role_name) VALUES ('manager');
INSERT INTO tomcat_users_roles (user_name, role_name) VALUES ('deron', 'dude');
INSERT INTO tomcat_users_roles (user_name, role_name) VALUES ('deron', 'manager');
INSERT INTO tomcat_users_roles (user_name, role_name) VALUES ('larry', 'dude');

select * from tomcat_users;
select * from tomcat_users_roles